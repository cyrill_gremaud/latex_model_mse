
# Template-LaTeX

LaTeX template for MSE semester, deepening project, thesis reports.

## Getting Started

This template provides a LaTeX class which simply replaces and extends the standard directive `report`.

### Without Git
This template is based on (https://github.com/njakob/template-latex/releases) and modified for my needs. Thanks to Nicolas Jakob

This updated model is available at (https://bitbucket.org/cyrill_gremaud/latex_model_mse)

### What's new ?
The main differences with this of Nicolas Jakobs are:

- Added new type of report (deepening)
- Remove tab space when start a new paragraph with `\\` latex command. Just comment `\usepackage[parfill]{parskip}` in "/template-latex/mse-thesis.cls" if you don't want this feature
- Add a new bibliography backend "bibtex8" instead of "biber" for compatibility problem. The old option is just commented
- Add new command to add glossary and acronyms (`\insertglossary`)
- Modified structure example to match with the most frequent MSE report structure
- Add example using dirtree package for CD-ROM listing
- Add Minted code styling option

*It is recommended that you configure your workspace as shown below, then it will be easy to keep it updated.*

### Clone this Repository

Create a new folder for your report, then run git clone command.

```shell
$ git clone git@bitbucket.org/cyrill_gremaud/latex_model_mse.git
```

Now you can start to write your report in `thesis.tex`.

### Setup your Own Repository

Create a new folder for your report, and then initialize your repository as usual.

```shell
$ git init
$ git remote add origin <remote-location-of-your-git>
```

Now you can start to write your report in `thesis.tex`.

## Options

Different options can be defined for the document class.

### Confidential

Type: `Boolean` Default: False

Place a confidential marker on the first page.

### Lang

Type: `String` Default: `en`
Accept: `fr`, `en`

Language of your document.

### Major

Type: `String` Default: `tin`
Accept: `tic`, `tin` 

Define your major, `tic` for *Information and Communication Technologies* or `tin` for *Industrial Technologies*.

### Path

Type: `String`

Path to this document class.

### Type

Type: `String` Default: `thesis`
Accept: `semester`, `thesis`, `deepening`

Define whether it is a semester project, thesis, or a deepening project (PA).

### useMintet

Type: `String`
Accept: `yes`,`no`

Define whether you want to use Minted to have cool and fun coding style.
If yes, please read instruction below to install needed stuff.

## Metadata

Some metadata listed below must be provided to the document class.

### Advisor

Advisor of your project, it is usually the same person as `\professor`.

Example: `\advisor{Prof. John Doe}`

### Author

Your full name.

Example: `\author{Cyrill Gremaud}`

### Contact

Your e-mail address.

Example: `\contact{gremaudc@gmail.com}`

### Expert

Name and contact information of your expert(s).

Example:
```latex
\expert{
   Prof. John Doe
   \institute{HES-SO}
   \email{john.doe@nowhere.com}
}
```

### HeadOfMSE

Name of Head of MSE.

Example: `\headofmse{Fariba Moghaddam Bützberger}`

### Location

*Optional*

When you are abroad, place the information about the school or institute where you have done your work.

Example:
```latex
\location{
   Institute which is aboard
   \institute{HES-SO}
}
```

### Title

Title of your report.

Example: `\title{\LaTeX\ Model MSE}`

### Professor

Professor(s) who follow your research.

Example:
```latex
\professor{
   Prof. John Doe
   \institute{HES-SO}
   \email{john.doe@nowhere.com}
   \and
   Prof. John Doe
   \institute{HES-SO}
   \email{john.doe@nowhere.com}
}
```

### ProposedBy

Person or company who proposed the subject of your work.

Example:
```latex
\proposedby{
   Nicolas Jakob
   \institute{HES-SO}
   \email{nicolas.jakob@master.hes-so.ch}
}
```

### Supervisor

*Optional*

When you are abroad, place information about the person following your research.

Example:
```latex
\supervisor{
   Prof. John Doe
   \institute{HES-SO}
   \email{john.doe@nowhere.com}
}
```

### Version

Keep track of the published version of your document.

Example: `\version{Version 1}`

### Installation notes for Ubuntu 14.10

To compile this template on Ubuntu, some additional package must be installed. Please note that this template has always been compilated with TexStudio

```shell
sudo apt-get install texstudio texlive-generic-extra texlive-latex-extra texlive-bibtex-extra biber texlive-fonts-recommended 
```

Please inform me if I have forgotten a package.

### Install Minted

To use Minted coding style, it is necessary to install python and pip. Once done, use pip to install pygments.
Please be sure to have C:\%PythonInstallDir%\Scripts in your PATH.

```shell
python -m pip install -U pygments
```

In your Latex editor, you must add a compilation parameter : -shell-escape

For Linux, it is easier. Please refer to (https://pip.pypa.io/en/latest/installing.html) and (https://github.com/gpoore/minted)

### Some useful examples

To insert a picture : The option `width=\textwidth` fit the picture according to the width of the text.

```latex
\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{includes/pictures/yourpicture.png}
	%\includegraphics[scale=0.5]{includes/pictures/yourpicture.png}
	\caption{my cool picture}
	\label{my cool picture}
\end{figure}
```

To insert 2 pictures side by side

```latex
\begin{figure}[h]
	\begin{minipage}[b]{.5\textwidth}
		\centering\includegraphics[width=\textwidth]{includes/pictures/yourpicture}
		\caption{My cool picture 1\label{coolpic1}}
	\end{minipage}
	\begin{minipage}[b]{.5\textwidth}
		\centering\includegraphics[width=\textwidth]{includes/pictures/yourpicture}
		\caption{My cool picture 2\label{coolpic2}}
	\end{minipage}
\end{figure}
```

To insert a classic code listing
```latex
\begin{lstlisting}[language=bash, frame=single, caption="Check java version", captionpos=b]
~# java -version
java version "1.7.0_80"
Java(TM) SE Runtime Environment (build 1.7.0_80-b15)
Java HotSpot(TM) 64-Bit Server VM (build 24.80-b11, mixed mode)
\end{lstlisting}
```

To insert a Minted code listing
```latex
% require useMinted=yes in thesis.tex
\begin{minted}[mathescape, linenos, numbersep=5pt, frame=lines, framesep=2mm]{csharp}
string title = "This is a Unicode in the sky"
/*
Defined as $\pi=\lim_{n\to\infty}\frac{P_n}{d}$ where $P$ is the perimeter
of an $n$-sided regular polygon circumscribing a
circle of diameter $d$.
*/
const double pi = 3.1415926535
\end{minted}
```

